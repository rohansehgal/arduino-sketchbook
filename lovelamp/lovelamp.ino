
float getTemp()
{
   int raw_val = analogRead(A0);
   float voltage = (raw_val/1024.0) * 5.0;
   float temp = (voltage - 0.5) * 100;
   return temp;
}

float ambient_temp = 21.0;

int blue = 255;
int red = 0;
int green = 0;

void setup()
{
   Serial.begin(9600);
   ambient_temp = getTemp();
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop()
{ 
   float current_temp = getTemp();
   //Serial.println(current_temp);
   
   //we set the blue = 255 at ambient, for each .01 celsius change we subtract 1
   //from blue and add to red.

    int delta = (current_temp - ambient_temp) * 100;
    
    if (delta > 255)
      delta = 255; 

    if (delta < 0)
       delta = 0;

    blue = 255 - delta;
    red = delta;
    
    analogWrite(9, red);
    analogWrite(10, green);
    analogWrite(11, blue);
}
