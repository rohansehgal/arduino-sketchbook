
float getTemp()
{
   int raw_val = analogRead(A0);
   float voltage = (raw_val/1024.0) * 5.0;
   float temp = (voltage - 0.5) * 100;
   return temp;
}

float ambient_temp = 21.0;

void setup()
{
   Serial.begin(9600);
   ambient_temp = getTemp();
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}



void loop()
{ 
   float current_temp = getTemp();
   //Serial.println(current_temp);

   digitalWrite(3, LOW);
   digitalWrite(4, LOW);
   digitalWrite(5, LOW);

   if (current_temp > ambient_temp + 1)
   {
      digitalWrite(3, HIGH);
      digitalWrite(4, LOW);
      digitalWrite(5, LOW);
   }
   if (current_temp > ambient_temp + 2)
   {
      digitalWrite(3, HIGH);
      digitalWrite(4, HIGH);
      digitalWrite(5, LOW);
   }
   if (current_temp > ambient_temp + 3)
   {
      digitalWrite(3, HIGH);
      digitalWrite(4, HIGH);
      digitalWrite(5, HIGH);
   }
   delay(1);
}
