
int low = 0, high = 1023;

const int ledPin = 13, piezoPin = 8;

void setup()
{
   Serial.begin(9600);
   int prval;
   pinMode(ledPin, OUTPUT);
   
   digitalWrite(ledPin, HIGH);
   
   while(millis() < 5000)
   {
      prval = analogRead(A0);
      if (prval > high)
         high = prval;
      if (prval < low)
         low = prval;
   }

   digitalWrite(ledPin, LOW);
}

void loop()
{
   int prval;
   prval = analogRead(A0);
   int pitch = map(prval, low, high, 50, 4000);
   tone(8, pitch, 20);
   Serial.println(map(prval, low, high, 0, 100));
   delay(10);
}
