import serial
import alsaaudio

port = serial.Serial("/dev/ttyACM0", baudrate=9600, timeout=3.0)
m = alsaaudio.Mixer()

while True:
    rcv = port.readline(10)
    rcv.strip()
    if rcv:
        m.setvolume(int(rcv))
